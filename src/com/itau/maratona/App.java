package com.itau.maratona;

import java.util.List;

public class App {

	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");
	
		List<Aluno> alunos  = arquivo.ler();
		
		int tamanhoAlunos = alunos.size();
		int quantidadeEquipes = tamanhoAlunos / 3;
		
		List<Equipe> equipe = CriadorEquipes.construirEquipe(quantidadeEquipes, alunos);
		
		Impressora.imprimirEquipe(equipe);

		
	}

}
