package com.itau.maratona;

import java.util.List;
import java.util.ArrayList;

public class CriadorEquipes {

	public static List<Equipe> construirEquipe(int quantidadeEquipes, List<Aluno> aluno) {
		List<Equipe> equipes = new ArrayList<>();

		for (int i = 0; i < quantidadeEquipes; i++) {
			Equipe equipe = new Equipe();
			equipe.idEquipe = i;
			equipe.alunos = sortearAlunos(aluno);
			equipes.add(equipe);
		}

		return equipes;
	}

	private static List<Aluno> sortearAlunos(List<Aluno> aluno) {
		List<Aluno> alunosSorteados = new ArrayList<>();
		
		for (int i = 0; i < 3; i++) {
			Double valor = (Math.random() * aluno.size());
			alunosSorteados.add(aluno.get(valor.intValue()));			
			aluno.remove(valor.intValue());
		}

		return alunosSorteados;
	}

}
