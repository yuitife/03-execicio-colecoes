package com.itau.maratona;

import java.util.List;

public class Equipe {
	public int idEquipe;
	public List<Aluno> alunos;

	public String toString() {
		String fraseFinal = new String();

		fraseFinal = "Equipe: " + idEquipe +"\n";

		for (int i = 0; i < 3; i++) {
			fraseFinal += alunos.get(i) + "\n";
		}
		return fraseFinal;

	}

}
